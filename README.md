# turtlebot_gosquare

**A simple ROS node that moves the turtlebot in GazeboSim.**

This ROS node subscribes to the bumper status topic and publishes velocity commands (twist).
If no collision happen, the turtlebot will turn right after a given period of time.
If a collision happen, the turtlebot will turn with regard of which bumper was hit.

**Usage:**


0. Create a workspace named *test_ws* 
```
$ mkdir test_ws
$ cd test_ws
/test_ws$ mkdir src
/test_ws$ mkdir devel
/test_ws$ mkdir build
/test_ws$ catkin_make
```

1. Clone from GitLab and build the package from your workspace
```
/test_ws$ cd src
/test_ws/src$ git clone https://gitlab.com/dumbrobot00/turtlebot_gosquare.git
/test_ws/src$ cd ..
/test_ws$ catkin_make
```

2. Start another terminal and launch turtlebot_gazebo
```
roslaunch turtlebot_gazebo turtlebot_world.launch`
```

3. In the terminal where you built the turtlebot_gosquare node
```
/test_ws$ source devel/setup.bash
/test_ws$ rosrun turtlebot_gosquare turtlebot_gosquare_node
```

You should see the turtlebot in gazebosim running
